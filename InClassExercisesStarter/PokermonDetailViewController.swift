//
//  PokermonDetailViewController.swift
//  InClassExercisesStarter
//
//  Created by Sukhwinder Rana on 2018-12-02.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Alamofire
import SwiftyJSON
import CoreLocation


class PokermonDetailViewController:UIViewController,CLLocationManagerDelegate  {
    var name = ""
    var rowImage = ""
    var r = 0
    //var items = ["Mew", "Pikachu", "Squirtle","Zubar"]
    
    @IBOutlet weak var labelSucessMessage: UILabel!
    @IBOutlet weak var pokemonDetailLabel: UILabel!
    @IBOutlet weak var pokemonImage: UIImageView!
    //@IBOutlet weak var lblResult: UILabel!
    // Mark: Firestore variables
    var db:Firestore!
    var bc = ""
    var manager:CLLocationManager!
    @IBAction func buttonSelectPokemon(_ sender: Any) {
        
    }
    // MARK: Default Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        //let name = self.name
        db = Firestore.firestore()
        let ref = db.collection("Pokemon").whereField("name", isEqualTo: self.name)
        ref.getDocuments() {
            (querySnapshot, err) in
            if (err == nil){
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                    self.labelSucessMessage.text! = "\(document.data())"
                    print("--------")
                    print("--------")
                    print("--------")
                    print(self.rowImage)
                    print("--------")
                    print("--------")
                    print("--------")
                    self.pokemonImage.image = UIImage(named: self.rowImage)
                }
            }
            else if let err = err {
                print("this restaurant is not in database")
                self.labelSucessMessage.text! = "Error getting documents: \(err)"
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
